#Fresh Air Bike Route#

This project is part of [Information, Logic and Languages](http://www.fer.unizg.hr/en/course/ilal) course (Bachelor study program,6th semester, [Faculty of Electrical Engineering and Computing (FER)](http://www.fer.unizg.hr/en)).

#Authors:#

* Filip Dujmušić - `0036478548` 
* Matija Pevec - `0036479246`

# Description#

Java Swing application made for optimizing bike routes in purpose to decrease the level of air pollution. 

The prime goal is bike routing through the city where, if possible, smaller streets will be chosen over bigger ones (estimating that smaller types of roads will have less vehicle flow and the air will be cleaner).

For implementation of this application we used [Swing](https://en.wikipedia.org/wiki/Swing_\(Java\)) based [Mapsforge](http://wiki.openstreetmap.org/wiki/Mapsforge) library that uses **.map** files as a map source (**.map** files are made from encoding **.xml**/**.osm** files). Route optimizations and calculations are made through the [GraphHopper](http://wiki.openstreetmap.org/wiki/GraphHopper) library where we use [bidirectional](https://en.wikipedia.org/wiki/Bidirectional_search) [Dijkstra algorithm](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm) with custom graph weights depending on the [road type](http://wiki.openstreetmap.org/wiki/Key:highway). 

Weights can be changed in `BikeNarrowStreetWeighting.java` file, for example:


```
#!java

weightMap.put("path", 2.);
weightMap.put("footway", 5.);
weightMap.put("cycleway", 5.);
weightMap.put("tertiary", 12.);
weightMap.put("secondary", 15.);
weightMap.put("primary", 30.);
weightMap.put("trunk", 90.);
weightMap.put("motorway", 100.);
```

# Usage#

If you run the program for the first time, it may take about 2 minutes (depending on your system performance) to generate and store GraphHopper files needed for fast calculations. After that, the map screen should appear. 

**First step** is defining the starting point of your route by clicking with **left mouse button** on the map. The *green marker* will appear on the pointed location.
**Second step** is defining the ending point of your route by clicking with **left mouse button** on the map. The *red marker* will appear on the pointed location.
**Third step** is showing the route on the map.

After defining the ending point, two different coloured routes will be shown :

* **Red route** - Dijkstra algorithm without any graph weights
* **Blue route** - Dijkstra algorithm with the weights that depend on road type (preferring smaller streets)

In every step is able to reset your queries by clicking **ESCAPE** button on the keyboard which will take you to the first step.

![bike-optimization-route](https://bitbucket.org/repo/8ajB7A/images/362204588-bike.gif)


#Installation#

* Download [croatia.map](https://bitbucket.org/mightymatth8/freshairbikeroute/downloads/croatia.map) and [croatia.osm](https://bitbucket.org/mightymatth8/freshairbikeroute/downloads/croatia.osm)

* Put downloaded files in directory `src/main/resources/maps/`

* Data location for **GraphHopper** library is located in the system's
default `temp` directory. Take a look [here](https://en.wikipedia.org/wiki/Temporary_folder)
for more information.