package hr.fer.ilj.project;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by filip on 13.06.16..
 */
public class MapViewerTests {

    @Test(expected = IllegalArgumentException.class)
    public void getMapFileTestException() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = MapViewer.class.getDeclaredMethod("getMapFile", String.class);
        method.setAccessible(true);
        method.invoke(MapViewer.class, null);
    }

    @Test
    public void getMapFileTestNoException() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = MapViewer.class.getDeclaredMethod("getMapFile", String.class);
        method.setAccessible(true);
        method.invoke(MapViewer.class, MapViewer.class.getClassLoader().getResource("maps/croatia.map").getPath());
    }



}
