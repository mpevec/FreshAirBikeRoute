package hr.fer.ilj.project;

import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.routing.AlgorithmOptions;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.util.PointList;
import org.mapsforge.core.graphics.*;
import org.mapsforge.core.graphics.Color;
import org.mapsforge.core.graphics.Paint;
import org.mapsforge.core.model.BoundingBox;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.model.MapPosition;
import org.mapsforge.core.util.LatLongUtils;
import org.mapsforge.map.awt.graphics.AwtBitmap;
import org.mapsforge.map.awt.graphics.AwtGraphicFactory;
import org.mapsforge.map.awt.util.JavaPreferences;
import org.mapsforge.map.awt.view.MapView;
import org.mapsforge.map.layer.Layer;
import org.mapsforge.map.layer.Layers;
import org.mapsforge.map.layer.cache.FileSystemTileCache;
import org.mapsforge.map.layer.cache.InMemoryTileCache;
import org.mapsforge.map.layer.cache.TileCache;
import org.mapsforge.map.layer.cache.TwoLevelTileCache;
import org.mapsforge.map.layer.debug.TileCoordinatesLayer;
import org.mapsforge.map.layer.debug.TileGridLayer;
import org.mapsforge.map.layer.download.TileDownloadLayer;
import org.mapsforge.map.layer.download.tilesource.OpenStreetMapMapnik;
import org.mapsforge.map.layer.download.tilesource.TileSource;
import org.mapsforge.map.layer.overlay.Marker;
import org.mapsforge.map.layer.overlay.Polyline;
import org.mapsforge.map.layer.renderer.TileRendererLayer;
import org.mapsforge.map.model.MapViewPosition;
import org.mapsforge.map.model.Model;
import org.mapsforge.map.model.common.PreferencesFacade;
import org.mapsforge.map.reader.MapFile;
import org.mapsforge.map.reader.ReadBuffer;
import org.mapsforge.map.rendertheme.InternalRenderTheme;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

/**
 * Class that makes GUI map that supports dragging, zooming, choosing starting point,
 * choosing ending point, calculating ways and drawing paths on the map.
 *
 * @author Matija Pevec
 * @author Filip Dujmušić
 */
public class MapViewer {
    private static final GraphicFactory GRAPHIC_FACTORY = AwtGraphicFactory.INSTANCE;
    private static final boolean SHOW_DEBUG_LAYERS = false;
    public static final int MARKER_WIDTH = 40;
    public static final int MARKER_HEIGHT = 40;
    public static final int FRAME_WIDTH = 800;
    public static final int FRAME_HEIGHT = 600;
    public static final int STROKE_WIDTH = 3;
    public static final double READ_BUFFER_SIZE = 1E7;
    public static final String ALGORITHM = AlgorithmOptions.DIJKSTRA_BI;
    public static final String VEHICLE_TYPE = "bikeCustom";
    public static final LatLong FER_LOCATION = new LatLong(45.80148277951885, 15.97112175708197);
    public static final String GRAPHHOPPER_LOCATION = System.getProperty("java.io.tmpdir")
            + File.separatorChar + "graphhopper";

    private final MapView mapView;
    private UserState userState;
    private Marker startMarker;
    private Marker endMarker;
    private Polyline optimizedDijkstraRoute;
    private Polyline dijkstraRoute;
    private BikeGraphHopper gHopper;


    public MapViewer() {

        userState = UserState.CHOOSING_START_POINT;
        gHopper = initGraphHopper(gHopper);

        ReadBuffer.setMaximumBufferSize((int) READ_BUFFER_SIZE);

        List<File> mapFiles = new ArrayList<>();
        mapFiles.add(getMapFile(Main.class.getClassLoader().getResource("maps/croatia.map").getPath()));

        mapView = createMapView();
        final BoundingBox boundingBox = addLayers(mapView, mapFiles);

        final PreferencesFacade preferencesFacade =
                new JavaPreferences(Preferences.userNodeForPackage(MapViewer.class));

        final JFrame frame = new JFrame();
        frame.setTitle("Fresh Air Bike Route Optimizer");
        frame.add(mapView);
        frame.pack();
        frame.setSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                int result = JOptionPane.showConfirmDialog(frame,
                        StringConstants.MESSAGE, StringConstants.TITLE, JOptionPane.YES_NO_OPTION);
                if (result == JOptionPane.YES_OPTION) {
                    mapView.getModel().save(preferencesFacade);
                    mapView.destroyAll();
                    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                }
            }

            @Override
            public void windowOpened(WindowEvent e) {
                final Model model = mapView.getModel();
                byte zoomLevel = LatLongUtils.zoomForBounds(model.mapViewDimension.getDimension(),
                        boundingBox, model.displayModel.getTileSize());
                //model.mapViewPosition.setMapPosition(new MapPosition(boundingBox.getCenterPoint(), zoomLevel));
                model.mapViewPosition.setMapPosition(new MapPosition(FER_LOCATION, zoomLevel));


            }
        });
        frame.setVisible(true);


        /**
         * Defining the mouse listener.
         *
         * Left Click - switching through the user states (CHOOSING_START_POINT,
         *              CHOOSING_END_POINT, SHOWING_ROUTE)
         */
        mapView.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == 1) {
                    LatLong latLong;
                    switch (userState) {
                        case CHOOSING_START_POINT:
                            latLong = mapView.getMapViewProjection().fromPixels(e.getX(), e.getY());
                            System.out.println(latLong);
                            startMarker = drawMarker(latLong, true);
                            userState = UserState.CHOOSING_END_POINT;
                            System.out.println("Start point has been set!");
                            break;
                        case CHOOSING_END_POINT:
                            latLong = mapView.getMapViewProjection().fromPixels(e.getX(), e.getY());
                            System.out.println(latLong);
                            endMarker = drawMarker(latLong, false);
                            userState = UserState.SHOWING_ROUTE;
                            System.out.println("End point has been set!");
                            dijkstraRoute = drawPath(startMarker.getLatLong(),
                                    endMarker.getLatLong(), "unoptimized", Color.RED);
                            optimizedDijkstraRoute = drawPath(startMarker.getLatLong(),
                                    endMarker.getLatLong(), "optimized", Color.BLUE);
                            break;
                        case SHOWING_ROUTE:
                            System.out.println("Showing route!");
                            break;
                    }

                }

            }
        });

        /**
         * Defining the key listener.
         * ESCAPE - clears the screen and setting user in the
         *          initial state (CHOOSING_START_POINT).
         */
        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    mapView.getLayerManager().getLayers().remove(startMarker);
                    mapView.getLayerManager().getLayers().remove(endMarker);
                    mapView.getLayerManager().getLayers().remove(optimizedDijkstraRoute);
                    mapView.getLayerManager().getLayers().remove(dijkstraRoute);
                    userState = UserState.CHOOSING_START_POINT;
                    System.out.println("Exiting showing the route!");
                }
            }
        });
    }

    /**
     * Initializing graphHopper object with necessary methods
     *
     * @return initialized graphHopper object
     */
    private static BikeGraphHopper initGraphHopper(BikeGraphHopper gHopper) {
        gHopper = (BikeGraphHopper) new BikeGraphHopper().forDesktop();
        gHopper.setCHEnable(false);
        gHopper.setOSMFile(Main.class.getClassLoader().getResource("maps/croatia.osm").getPath());
        gHopper.setGraphHopperLocation(new File(GRAPHHOPPER_LOCATION).toString());
        gHopper.setEncodingManager(new EncodingManager(new BikeCustomEncoder(4, 2, 0)));
        gHopper.importOrLoad();
        return gHopper;
    }


    /**
     * Draws a marker on the map. There are two types of supported markers: <b>green marker</b>
     * is used for starting point of the route and <b>red marker</b> is used for ending point of the route.
     *
     * @param latLong       coordinates where marker will be located
     * @param isStartMarker if true, <b>green marker</b> will be set on the screen, otherwise
     *                      the <b>red marker</b> will be set.
     * @return Marker reference on the marker (necessary for cleaning the screen after use)
     */
    private Marker drawMarker(LatLong latLong, boolean isStartMarker) {

        Bitmap markerIcon = null;
        try {
            if (isStartMarker) {
                markerIcon = new AwtBitmap(ImageIO.read(Main.class.getClassLoader()
                        .getResource("img/map-start-marker-icon.png")));
            } else {
                markerIcon = new AwtBitmap(ImageIO.read(Main.class.getClassLoader()
                        .getResource("img/map-end-marker-icon.png")));
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        markerIcon.scaleTo(MARKER_WIDTH, MARKER_HEIGHT);

        Marker marker = new Marker(latLong, markerIcon, 0, -markerIcon.getHeight() / 2);
        mapView.getLayerManager().getLayers().add(marker);

        return marker;

    }


    /**
     * Makes request with certain parameters such as algorithm and vehicle type.
     * Draws path on the map.
     *
     * @param start        starting point
     * @param end          ending point
     * @param dijkstraType if "optimized" passed, weightings from BikeNarrowStreetWeighting.java
     *                     class will be added. Else, if anything else passed, no weightings will
     *                     be added.
     * @param lineColor    color of the line added on the map
     * @return Polyline reference on the path (necessary for cleaning the screen after use)
     */
    private Polyline drawPath(LatLong start, LatLong end, String dijkstraType, Color lineColor) {
        GHRequest req = new GHRequest(start.getLatitude(), start.getLongitude(), end.getLatitude(), end.getLongitude());

        if (dijkstraType.equals("optimized")) {
            req.setAlgorithm(ALGORITHM).setWeighting("NARROW_STREET").setVehicle(VEHICLE_TYPE);
        } else {
            req.setAlgorithm(ALGORITHM).setVehicle(VEHICLE_TYPE);
        }

        GHResponse resp = gHopper.route(req);
        PointList bestRoute = resp.getBest().getPoints();

        // instantiating the paint object
        Paint paint = GRAPHIC_FACTORY.createPaint();
        paint.setColor(lineColor);
        paint.setStrokeWidth(STROKE_WIDTH);
        paint.setStyle(Style.STROKE);

        Polyline polyline = new Polyline(paint, GRAPHIC_FACTORY);
        List<LatLong> coordinateList = polyline.getLatLongs();

        int bestRouteSize = bestRoute.size();
        for (int i = 0; i < bestRouteSize; i++) {
            coordinateList.add(new LatLong(bestRoute.getLatitude(i), bestRoute.getLongitude(i)));
        }

        System.out.println("Distance: " + resp.getBest().getDistance() / 1000
                + " km\n"
                + "# of nodes in path: " + polyline.getLatLongs().size());
        mapView.getLayerManager().getLayers().add(polyline);
        return polyline;
    }


    /**
     * Adds the mapFiles' layers on the map.
     *
     * @param mapView  map object where layers are being added.
     * @param mapFiles list of the map files with their layers that will be added on the map.
     * @return bounding box with layers from mapFiles
     */
    private static BoundingBox addLayers(MapView mapView, List<File> mapFiles) {
        Layers layers = mapView.getLayerManager().getLayers();

        // layers.add(createTileDownloadLayer(tileCache, mapView.getModel().mapViewPosition));
        BoundingBox result = null;
        for (int i = 0; i < mapFiles.size(); i++) {
            File mapFile = mapFiles.get(i);
            TileRendererLayer tileRendererLayer = createTileRendererLayer(createTileCache(i),
                    mapView.getModel().mapViewPosition, true, true, true, mapFile);
            BoundingBox boundingBox = tileRendererLayer.getMapDataStore().boundingBox();
            result = result == null ? boundingBox : result.extendBoundingBox(boundingBox);
            layers.add(tileRendererLayer);
        }
        if (SHOW_DEBUG_LAYERS) {
            layers.add(new TileGridLayer(GRAPHIC_FACTORY, mapView.getModel().displayModel));
            layers.add(new TileCoordinatesLayer(GRAPHIC_FACTORY, mapView.getModel().displayModel));

        }
        return result;
    }

    /**
     * Creating a MapView object.
     *
     * @return MapView object that has been created.
     */
    private static MapView createMapView() {
        MapView mapView = new MapView();
        mapView.getMapScaleBar().setVisible(true);
        if (SHOW_DEBUG_LAYERS) {
            mapView.getFpsCounter().setVisible(true);
        }

        return mapView;
    }

    private static TileCache createTileCache(int index) {
        TileCache firstLevelTileCache = new InMemoryTileCache(128);
        File cacheDirectory = new File(System.getProperty("java.io.tmpdir"), "mapsforge" + index);
        TileCache secondLevelTileCache = new FileSystemTileCache(1024, cacheDirectory, GRAPHIC_FACTORY);
        return new TwoLevelTileCache(firstLevelTileCache, secondLevelTileCache);
    }

    @SuppressWarnings("unused")
    private static Layer createTileDownloadLayer(TileCache tileCache, MapViewPosition mapViewPosition) {
        TileSource tileSource = OpenStreetMapMapnik.INSTANCE;
        TileDownloadLayer tileDownloadLayer = new TileDownloadLayer(tileCache,
                mapViewPosition, tileSource, GRAPHIC_FACTORY);
        tileDownloadLayer.start();
        return tileDownloadLayer;
    }

    private static TileRendererLayer createTileRendererLayer(
            TileCache tileCache,
            MapViewPosition mapViewPosition,
            boolean isTransparent, boolean renderLabels, boolean cacheLables, File mapFile) {
        TileRendererLayer tileRendererLayer = new TileRendererLayer(tileCache,
                new MapFile(mapFile), mapViewPosition, isTransparent,
                renderLabels, cacheLables, GRAPHIC_FACTORY);
        tileRendererLayer.setXmlRenderTheme(InternalRenderTheme.OSMARENDER);
        return tileRendererLayer;
    }

    private static File getMapFile(String arg) {
        File mapFile = new File(arg);
        if (!mapFile.exists()) {
            throw new IllegalArgumentException("file does not exist: " + mapFile);
        } else if (!mapFile.isFile()) {
            throw new IllegalArgumentException("not a file: " + mapFile);
        } else if (!mapFile.canRead()) {
            throw new IllegalArgumentException("cannot read file: " + mapFile);
        }
        return mapFile;
    }

}
