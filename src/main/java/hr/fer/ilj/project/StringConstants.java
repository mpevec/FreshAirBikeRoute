package hr.fer.ilj.project;

/**
 * This class contains all strings used in entire application.
 *
 * @author Matija Pevec
 * @author Filip Dujmušić
 */

public class StringConstants {
    public static final String MESSAGE = "Are you sure you want to exit the application?";
    public static final String TITLE = "Confirm close";
}
