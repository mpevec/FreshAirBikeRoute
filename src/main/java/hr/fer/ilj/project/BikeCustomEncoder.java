package hr.fer.ilj.project;

import com.graphhopper.reader.OSMWay;
import com.graphhopper.routing.util.BikeCommonFlagEncoder;
import com.graphhopper.util.EdgeIteratorState;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;


/**
 * Class that defines a custom bike encoder. In addition to BikeCommonFlagEncoder,
 * it stores road types on the disk.
 *
 * @author Filip Dujmušić
 * @author Matija Pevec
 */
public class BikeCustomEncoder extends BikeCommonFlagEncoder {

    private static final String FILE_NAME = "pathToEdgeMapFile";
    public static final String encoderName = "bikeCustom";

    public Map<Integer, String> edgeToHighWayMap = new HashMap<>();

    protected BikeCustomEncoder(int speedBits, double speedFactor, int maxTurnCosts) {
        super(speedBits, speedFactor, maxTurnCosts);
    }

    @Override
    public void applyWayTags(OSMWay way, EdgeIteratorState edge) {
        super.applyWayTags(way, edge);
        int edgeId = edge.getEdge();
        String highwayType = way.getTag("highway");
        edgeToHighWayMap.put(edgeId, highwayType);
    }


    @Override
    public String toString() {
        return encoderName;
    }

    public void load(String path) {
        try {
            edgeToHighWayMap = new HashMap<>();

            BufferedReader reader = Files.newBufferedReader(Paths.get(path).resolve(FILE_NAME));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] split = line.split(" ");
                edgeToHighWayMap.put(Integer.parseInt(split[0]), split[1]);
            }
        } catch (Exception ignore) {
        }
    }

    public void save(String path) {
        try {
            Path p = Paths.get(path).resolve(FILE_NAME);
            BufferedWriter bufferedWriter = Files.newBufferedWriter(p);
            for (Map.Entry<Integer, String> entry : edgeToHighWayMap.entrySet()) {
                Integer id = entry.getKey();
                String type = entry.getValue();
                bufferedWriter.write(id + " " + type + "\n");
            }
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (Exception ignore) {
        }
    }

}
