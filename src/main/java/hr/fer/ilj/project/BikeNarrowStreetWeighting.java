package hr.fer.ilj.project;

import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.util.Weighting;
import com.graphhopper.util.EdgeIteratorState;

import java.util.HashMap;
import java.util.Map;

/**
 * Class that defines weightings on the given edges and combines
 * them with the fastest way.
 *
 * @author Filip Dujmušić
 * @author Matija Pevec
 */
public class BikeNarrowStreetWeighting implements Weighting {

    private final Double DEFAULT_WEIGHT = 500.0;

    private final String narrowStreetWeightingName = "NARROW_STREET";
    private final BikeCustomEncoder encoder;
    private final double maxSpeed;

    private static Map<String, Double> weightMap = new HashMap<>();
    {
        weightMap.put("footway", 5.);
        weightMap.put("bridleway", 2.);
        weightMap.put("path", 2.);
        weightMap.put("cycleway", 110.);
        weightMap.put("lane", 2.);
        weightMap.put("service", 1.);
        weightMap.put("residential", 8.);
        weightMap.put("unclassified", 10.);
        weightMap.put("tertiary", 12.);
        weightMap.put("secondary", 15.);
        weightMap.put("primary", 30.);
        weightMap.put("trunk", 90.);
        weightMap.put("motorway", 100.);
    }

    public BikeNarrowStreetWeighting(BikeCustomEncoder encoder) {
        this.encoder = encoder;
        this.maxSpeed = encoder.getMaxSpeed();
    }

    @Override
    public double getMinWeight(double distance) {
        return distance / maxSpeed;
    }

    @Override
    public double calcWeight(EdgeIteratorState edgeState, boolean reverse, int prevOrNextEdgeId) {
        double speed = reverse ? encoder.getReverseSpeed(edgeState.getFlags()) : encoder.getSpeed(edgeState.getFlags());
        if (speed == 0)
            return Double.POSITIVE_INFINITY;

        String highwayType = encoder.edgeToHighWayMap.get(edgeState.getEdge());
        Double weight = weightMap.getOrDefault(highwayType, DEFAULT_WEIGHT);
        return (edgeState.getDistance() / speed) * weight;
    }

    @Override
    public FlagEncoder getFlagEncoder() {
        return encoder;
    }

    @Override
    public String getName() {
        return narrowStreetWeightingName;
    }

    @Override
    public boolean matches(String weightingAsStr, FlagEncoder encoder) {
        return weightingAsStr.equals(this.getName()) && encoder.equals(this.encoder);
    }

    @Override
    public String toString() {
        return this.getName();
    }

}
