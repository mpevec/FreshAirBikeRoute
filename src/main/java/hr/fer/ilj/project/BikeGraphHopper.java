package hr.fer.ilj.project;

import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.util.Weighting;
import com.graphhopper.routing.util.WeightingMap;

/**
 * Custom GraphHopper class. Adds custom weighting option. When program
 * is running for the first time, it pre-process needed files and save
 * them on the disk. If the files already exist, it loads them into the
 * RAM.
 *
 * @author Filip Dujmušić
 * @author Matija Pevec
 */
public class BikeGraphHopper extends GraphHopper {

    private EncodingManager em;
    private String graphHopperFolder;
    private boolean hasLoadFinished = false;

    @Override
    public GraphHopper setEncodingManager(EncodingManager em) {
        super.setEncodingManager(em);
        this.em = em;
        return this;
    }

    @Override
    public boolean load(String graphHopperFolder) {
        boolean b = super.load(graphHopperFolder);
        hasLoadFinished = true;
        //already stored -> should find neede file at given path
        if (b) {
            ((BikeCustomEncoder) em.getEncoder(BikeCustomEncoder.encoderName)).load(graphHopperFolder);
        } else {
            this.graphHopperFolder = graphHopperFolder;
        }
        return b;
    }

    @Override
    protected void postProcessing() {
        super.postProcessing();
        if (hasLoadFinished) {
            ((BikeCustomEncoder) em.getEncoder(BikeCustomEncoder.encoderName)).save(graphHopperFolder);
        }
    }

    @Override
    public Weighting createWeighting(WeightingMap weightingMap, FlagEncoder encoder) {
        String weighting = weightingMap.getWeighting();
        if ("NARROW_STREET".equalsIgnoreCase(weighting)) {
            return new BikeNarrowStreetWeighting((BikeCustomEncoder) encoder);
        } else
            return super.createWeighting(weightingMap, encoder);
    }
}
