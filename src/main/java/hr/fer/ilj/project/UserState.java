package hr.fer.ilj.project;

/**
 * Enum that defines all possible states where the user is able
 * to be.
 *
 * @author Matija Pevec
 * @author Filip Dujmušić
 */
public enum UserState {
    CHOOSING_START_POINT, CHOOSING_END_POINT, SHOWING_ROUTE;
}
