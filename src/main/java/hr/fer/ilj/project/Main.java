package hr.fer.ilj.project;

/**
 * Class that starts an application.
 *
 * @author Filip Dujmušić
 * @author Matija Pevec
 */
public class Main {

    public static void main(String[] args) {
        new MapViewer();
    }

    private Main() {
        throw new IllegalStateException();
    }

}
